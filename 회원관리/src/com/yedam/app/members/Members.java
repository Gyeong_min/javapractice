package com.yedam.app.members;

public class Members {
	private String memberId;
	private String memberPassword;
	private int memNo; //수강신청확인할때 memNo 불러와야 해서 작성함
	// memberRole : 0 - 관리자, 1 - 일반 
	private int memberRole;
	
	//  Members와 MemberInfoVO에 같은 DB에 저장된 값이기 때문에 하나로 합쳐서 해도 될것같음
	
	
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberPassword() {
		return memberPassword;
	}

	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}

	public int getMemNo() {
		return memNo;
	}

	public void setMemNo(int memNo) {
		this.memNo = memNo;
	}

	public int getMemberRole() {
		return memberRole;
	}

	public void setMemberRole(int memberRole) {
		this.memberRole = memberRole;
	}

	@Override
	public String toString() {
		String info = "";
		
		if(memberRole == 0) {
			info = "관리자 계정 : " + memberId;
		}else {
			info = "일반 계정 : " + memberId;
		}
		
		return info;
	}
	
	
}
