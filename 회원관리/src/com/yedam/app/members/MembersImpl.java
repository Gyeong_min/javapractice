package com.yedam.app.members;

import com.yedam.app.login.DAO;

public class MembersImpl extends DAO{
	
	//싱글톤
		private static MembersImpl instance = null;
		public static MembersImpl getInstance() {
			if(instance == null) {
				instance = new MembersImpl();
			}
			return instance;
		}
	
		
	public Members selectOne(Members member) {
		Members loginInfo = null;
		try {
			connect();
			String sql = "SELECT * FROM members WHERE member_id = '" + member.getMemberId() +"'"; 
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				//아이디 존재
				if(rs.getString("member_password").equals(member.getMemberPassword())){
					//비밀번호 일치
					// -> 로그인 성공
					loginInfo = new Members();
					loginInfo.setMemberId(rs.getString("member_id"));
					loginInfo.setMemberPassword(rs.getString("member_password"));
					loginInfo.setMemberRole(rs.getInt("member_role"));
					loginInfo.setMemNo(rs.getInt("mem_no"));
				}else {
					System.out.println("비밀번호가 일치하지 않습니다.");
				}
			}else {
				System.out.println("아이디가 존재하지 않습니다.");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}	
		return loginInfo;
	}


}
