package com.yedam.app.info;

import java.util.List;



public interface MemberInfoDAO{

	
	
		// 전체조회
		List<MemberInfoVO> selectAll();
		
		// 조건전체조회
		List<MemberInfoVO> selectOne(MemberInfoVO memIVO);
		
		//등록
		void insert(MemberInfoVO memIVO);
		
		
		//수정
		void update(MemberInfoVO memIVO);
		
		//삭제
		void delete(int memNo);

	
}
