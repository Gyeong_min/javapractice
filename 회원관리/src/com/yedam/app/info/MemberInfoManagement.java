package com.yedam.app.info;

import java.util.List;
import java.util.Scanner;

import com.yedam.app.login.Management;


public class MemberInfoManagement extends Management {
	

	Scanner sc =new Scanner(System.in);
	MemberInfoDAO midao = MemInDAOImpl.getInstance();
	
	public MemberInfoManagement() {
		while(true) {
			menuPrint();
			//메뉴 출력
			int menuNo = menuSelect();
			
			//각 메뉴의 기능을 실행
			if(menuNo == 1){
				//회원등록
				insertMemberInfo();
			}else if(menuNo ==2) {
				//특정화면조회
				selectOne();
			}else if(menuNo ==3) {
				//회원삭제
				deleteMemberInfo();
			}else if(menuNo ==4) {
				//전체회원조회
				selectAll();
			}else if(menuNo ==5 ) {
				//회원정보수정
				updateMemberInfo();
			}else if (menuNo == 6) {
				//종료
				end();
				break;
			}else {
				//기타사항
				printErrorMessage();
				
			}
		}
	}
	private void printErrorMessage() {
		System.out.println("======================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 한번 확인해주세요");
		System.out.println("======================");
	}
	private void end() {
		System.out.println("====================");
		System.out.println("프로그램을 종료합니다.");
		System.out.println("====================");
	}
	
	protected void menuPrint() {
		//해당 메뉴 출력
		System.out.println("=============================================================");
		System.out.println("1.회원등록 2.개인별 조회 3.회원삭제 4.전체회원 조회 5.회원정보수정 6.종료");
		System.out.println("=============================================================");
	}
	protected int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어 있습니다.");
		}
		return menuNo;
	}
	
	protected void selectAll() {
		List<MemberInfoVO> list = midao.selectAll();
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		for(MemberInfoVO memIVO : list) {
			System.out.printf("%d: %s, %s,%s,%s,%s \n", memIVO.getMemNo(), memIVO.getMemName(), memIVO.getAddress(),memIVO.getBirthDate(),memIVO.getPhoneNumber(),memIVO.getGender());
		}
	}
	
	protected void selectOne() {//동명이인이 있을 때 2개이상의 (중복값 )값을 출력하기 위해서 List 사용함
		MemberInfoVO findMemI = inputMemberInfo2();
		List<MemberInfoVO> list = midao.selectOne(findMemI);
		if(list. isEmpty()) {
			System.out.printf("%s 회원은 존재하지 않습니다. \n",findMemI.getMemName());
			return;
		}
		System.out.println("검색결과 > ");
		for(MemberInfoVO memIVO : list) {
			System.out.printf("%d: %s, %s,%s,%s,%s \n", memIVO.getMemNo(), memIVO.getMemName(), memIVO.getAddress(),memIVO.getBirthDate(),memIVO.getPhoneNumber(),memIVO.getGender());
		}
	}
	protected void insertMemberInfo() {
		MemberInfoVO memIVO = inputMemIAll();
		midao.insert(memIVO);
	}
	
	protected void updateMemberInfo() {
		MemberInfoVO memIVO = inputMemberInfo();
		midao.update(memIVO);
	}
	
	protected void deleteMemberInfo() {
		int memIVO = inputMemNo();
		midao.delete(memIVO);
	}
	
	protected MemberInfoVO inputMemIAll() {
		MemberInfoVO memIVO = new MemberInfoVO();
		System.out.println("회원번호 > ");
		memIVO.setMemNo(Integer.parseInt(sc.nextLine()));
		System.out.println("이름 > ");
		memIVO.setMemName(sc.nextLine());
		System.out.println("주소 > ");
		memIVO.setAddress(sc.nextLine());
		System.out.println("생년월일(yyyy-MM-dd)");
		memIVO.setBirthDate(sc.nextLine());
		System.out.println("성별(남자,여자) > ");
		memIVO.setGender(sc.nextLine());
		System.out.println("전화번호 > ");
		memIVO.setPhoneNumber(sc.nextLine());
		System.out.println("관리자 > ");
		memIVO.setMemberRole(Integer.parseInt(sc.nextLine()));
		System.out.println("아이디 > ");
		memIVO.setMemberId(sc.nextLine());
		System.out.println("비밀번호 > ");
		memIVO.setMemberPassword(sc.nextLine());
		
		return memIVO;
	}
	
	protected MemberInfoVO inputMemberInfo() {
		MemberInfoVO memIVO = new MemberInfoVO();
		
		System.out.println("회원번호 > ");
		memIVO.setMemNo(Integer.parseInt(sc.nextLine()));
		System.out.println("주소 > ");
		memIVO.setAddress(sc.nextLine());
		System.out.println("전화번호");
		memIVO.setPhoneNumber(sc.nextLine());
		return memIVO;
	}
	protected MemberInfoVO inputMemberInfo2() {
		MemberInfoVO memIVO = new MemberInfoVO();
		
		System.out.println("회원이름 > ");
		memIVO.setMemName(sc.nextLine());
		
		return memIVO;
	}
	
	protected int inputMemNo() {
		int memNo = 0;
		try {
			System.out.println("회원번호 > ");
			memNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("회원번호는 문자와 숫자로 구성되어 있습니다.");
		}
		return memNo;
	}
	
}

	

	