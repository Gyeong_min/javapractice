package com.yedam.app.info;

public class MemberInfoVO {
	private int memNo; 
	private String memName;
	private String address;
	private String birthDate;
	private String phoneNumber;
	private String memberId;
	private String memberPassword;
	private int memberRole;
	private String gender;
	
	//public static int memNo; 사용해서 오류 났었음 memNo 다 같은값으로 출력됨
	//static 키워드를 사용한 변수는 클래스가 메모리에 올라갈 때 자동으로 생성이 된다. 즉, 인스턴스(객체) 생성 없이 바로 사용가능 하다. 
	//인스턴스 생성 없이 바로 사용가능 하기 때문에 프로그램 내에서 공통으로 사용되는 데이터들을 관리 할 때 이용한다.
	//static은 결코 남발해서는 안되며 !공통으로 값을 유지하고 싶을 때만! 사용해야 한다.
	
	public int getMemNo() {
		return memNo;
	}
	
	public void setMemNo(int memNo) {
		this.memNo = memNo;
	}
	public String getMemName() {
		return memName;
	}
	public void setMemName(String memName) {
		this.memName = memName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberPassword() {
		return memberPassword;
	}
	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}
	public int getMemberRole() {
		return memberRole;
	}
	public void setMemberRole(int memberRole) {
		this.memberRole = memberRole;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "[회원번호 =" + memNo + ", 회원이름 =" + memName + ", 주소 =" + address + ", 생일 ="
				+ birthDate + ", 전화번호 =" + phoneNumber +", 성별 =" + gender + "]";
	}
	
	
	
}
