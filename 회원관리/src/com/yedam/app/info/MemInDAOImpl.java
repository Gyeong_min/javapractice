package com.yedam.app.info;


import java.util.ArrayList;
import java.util.List;

import com.yedam.app.login.DAO;

public class MemInDAOImpl extends DAO implements MemberInfoDAO {
	private static MemberInfoDAO instance = null;
	
	public static MemberInfoDAO getInstance() {
		if(instance == null)
				instance = new MemInDAOImpl();
			return instance;
		}
	
	@Override
	public List<MemberInfoVO> selectAll() {//전체조회
		List<MemberInfoVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM members";
			rs = stmt.executeQuery(sql);//rs = stmt.excuteQuery(SQL) - SQL 질의 결과를 ResultSet에 저장합니다.
			while(rs.next()) {
				MemberInfoVO memInVO = new MemberInfoVO();
				memInVO.setMemNo(rs.getInt("mem_no"));
				memInVO.setMemName(rs.getString("mem_name"));
				memInVO.setAddress(rs.getString("address"));
				memInVO.setBirthDate(rs.getString("birth_date"));
				memInVO.setPhoneNumber(rs.getString("phone_number"));
				memInVO.setGender(rs.getString("gender"));
				
				list.add(memInVO);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;
	}

	@Override
	public List<MemberInfoVO> selectOne(MemberInfoVO memIVO) {//개인별조회
		List<MemberInfoVO> list = new ArrayList<>();
		MemberInfoVO findVO = null;
		try {
			connect();
			stmt = conn.createStatement();
			
			String sql = "SELECT * FROM members WHERE mem_name = '" + memIVO.getMemName()+"'" ;
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				findVO = new MemberInfoVO();
				findVO.setMemNo(rs.getInt("mem_no"));
				findVO.setMemName(rs.getString("mem_name"));
				findVO.setAddress(rs.getString("address"));
				findVO.setBirthDate(rs.getString("birth_date"));
				findVO.setPhoneNumber(rs.getString("phone_number"));
				findVO.setGender(rs.getString("gender"));
				
				list.add(findVO);
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list ;
	}
	

	@Override
	public void insert(MemberInfoVO memIVO) {//멤버등록 //?와 밑에 값 갯수가 같아야함 그리고 DB 테이블에 저장된 순서로 적어야 올바른 위치에 값이 들어감!!!
		try {
			connect();
			String sql = "Insert INTO members VALUES (?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, memIVO.getMemNo());
			pstmt.setString(2, memIVO.getMemName());
			pstmt.setString(3, memIVO.getAddress());
			pstmt.setString(4, memIVO.getBirthDate());
			pstmt.setString(5, memIVO.getPhoneNumber());
			pstmt.setString(6, memIVO.getMemberId());
			pstmt.setString(7, memIVO.getMemberPassword());
			pstmt.setInt(8, memIVO.getMemberRole());
			pstmt.setString(9, memIVO.getGender());
			
			
			int result=pstmt.executeUpdate();
			
			if(result > 0) { 
				System.out.println("정상적으로 등록되었습니다.");
			}else {
				System.out.println("정상적으로 등록되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}

		
	}

	@Override
	public void update(MemberInfoVO memIVO) {//멤버수정
		try {
			connect();

			
			String sql = "UPDATE members SET  address = ? , phone_number = ? WHERE mem_no =? ";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memIVO.getAddress());
			pstmt.setString(2, memIVO.getPhoneNumber());
			pstmt.setInt(3,memIVO.getMemNo());
			
			int result = pstmt.executeUpdate();//UPDATE문에서는 executeUpdate()메소드를 사용
			
			if(result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("정상적으로 수정되지 않았습니다.");
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
	}

	@Override
	public void delete(int memNo) {//삭제
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM members WHERE mem_no = " + memNo;
			int result = stmt.executeUpdate(sql);
			if(result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			}else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}

		
	}

	

}
