package com.yedam.app.login;

import java.util.Scanner;

import com.yedam.app.Enrolment.EnrolmentDAO;
import com.yedam.app.Enrolment.EnrolmentManagement;
import com.yedam.app.info.MemberInfoDAO;
import com.yedam.app.info.MemberInfoManagement;
import com.yedam.app.members.MembersImpl;


public class Management {
	protected Scanner sc = new Scanner(System.in);
	
	public void run() {
		boolean role = selectRole();
		while(true) {
			menuPrint(role);
			
			int menuNo = menuSelect();
			
			if(menuNo == 2) {
				//회원등록
				new MemberInfoManagement();
			}else if(menuNo == 1 && role) {
				//수강신청
				new EnrolmentManagement();
			}else if(menuNo == 3) {
				//프로그램 종료
				exit();
				break;
			}else {
				//입력오류
				showInputError();
			}
		}
	}
	//메소드
	protected void menuPrint(boolean role) {
		//권한에 따른 메뉴
		String menu ="";
		if(role) {
			menu += "1.수강신청 "
				 + "3.로그아웃";
		}else if(role==false) {
			menu += "2.회원메뉴 "
					+"3.로그아웃";
		}
		
		
		System.out.println("===========================");
		System.out.println(menu);
		System.out.println("===========================");
	}
	
	protected int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("숫자를 입력해주시기 바랍니다.");
		}
		return menuNo;
	}
	
	protected void exit() {
		System.out.println("프로그램을 종료합니다.");
	}
	
	protected void showInputError() {
		System.out.println("메뉴에서 입력해주시기 바랍니다.");
	}
	
	protected boolean selectRole() {
		int memberRole = LoginControl.getLoginInfo().getMemberRole();
		if(memberRole == 1) {//0관리자
			return true;
		}else {
			return false;
		}
	}
		


}
