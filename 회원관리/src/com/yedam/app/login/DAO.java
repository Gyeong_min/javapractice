package com.yedam.app.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAO {
	//DataBase 연결정보
		private String jdbcDriver = "org.sqlite.JDBC";
//		private String jdbcUrl = "jdbc:sqlite:/D:/dev/database/MemberDataBase.db";
		private String jdbcUrl = "jdbc:sqlite:/C:/Users/admin/Desktop/MembersProject/MemberDataBase.db";
		//집//		private String jdbcUrl = "jdbc:sqlite:/C:/dev/database/MemberDataBase.db";
		
		//각 메소드에서 공통적으로 사용하는 변수 -> 필드
		protected Connection conn;
		protected Statement stmt;
		protected PreparedStatement pstmt;
		protected ResultSet rs;
		//아래에 자세한 설명있음
		
		//연결
		//첫번째 JDBC DRIVER LOADING
		//두번째 CONNECTION
		public void connect() {
			try {
				Class.forName(jdbcDriver);	
				conn = DriverManager.getConnection(jdbcUrl);			
			}catch(ClassNotFoundException e) {
				System.out.println("JDBC DRIVER LOADING FAIL");
			}catch(SQLException e) {
				System.out.println("DATABASE CONNECTION FAIL");
			}
		}
		
		
		//자원해제
		public void disconnect() {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch(SQLException e) {
				System.out.println("정상적으로 자원이 해제되지 않았습니다.");
			}
		}
}

//////////////////////////////
//Connection - 자바와 DB를 연결하는것, Statement ResultSet 얻으려면 필수적으로 있어야함!
// Statement와 PreparedStatement 차이
//PreparedStatement = 

//Statement를 사용하면 매번 쿼리를 수행할 때마다 단계를 거치게 되고(계속적으로 단계를 거치면서 수행)
//Statement = sql구문을 실행하는 역할 ,스스로 이해하지는 못하고 전달역할만 함
//PreparedStatement는 처음 한 번만 단계를 거친 후 캐시에 담아 재사용을 한다는 것이다. 만약 동일한 쿼리를 반복적으로 수행한다면 PreparedStatment가 DB에 훨씬 적은 부하를 주며, 성능도 좋다.

//캐시 사용여부가 가장 큰 차이 ,prepared=> 여러번사용시 , Statement =>한번사용시
// ResultSet - 결과값을 받을때 사용합니다.