package com.yedam.app.login;

import java.util.Scanner;

import com.yedam.app.members.Members;
import com.yedam.app.members.MembersImpl;



public class LoginControl {//static이있는 loginInfo필드에 싱글톤으로 로그인 정보 생성,저장 ->다른곳에서도 이용가능(ex 수강강좌확인할때 로그인한 회원번호 불러올때 사용
	private Scanner sc = new Scanner(System.in);//System.in 값을 입력받는 클래스
	private static Members loginInfo = null;
	public static Members getLoginInfo() {
		return loginInfo;
	}
	
	public LoginControl() {
		while(true) {
			menuPrint();			
			int menuNo = menuSelect();			
			if(menuNo == 1) {
				//로그인
				login();
			}else if(menuNo == 2) {
				//종료
				exit();
				break;
			}else {
				showInputError();
			}
		}
	}
	
	private void menuPrint() {
		System.out.println("==============");
		System.out.println("1.로그인  2.종료");
		System.out.println("==============");
	}
	
	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("숫자 형식으로 입력해주세요.");
		}
		return menuNo;
	}
	
	private void exit() {
		System.out.println("프로그램을 종료합니다.");
	}
	
	private void showInputError() {
		System.out.println("메뉴를 확인해주시기 바랍니다.");
	}
	
	private void login() {
		//아이디와 비밀번호 입력
		Members inputInfo = inputMember();
		//로그인
		loginInfo = MembersImpl.getInstance().selectOne(inputInfo);
		
		//실패하면 종료
		if(loginInfo == null) return;
		
		//성공하면 프로그램을 실행
		new Management().run();
	}
	
	private Members inputMember() {
		Members info = new Members();
		System.out.print("아이디 > ");
		info.setMemberId(sc.nextLine());
		System.out.print("비밀번호 > ");
		info.setMemberPassword(sc.nextLine());
		
		return info;
	}
	
}
