package com.yedam.app.Enrolment;

import java.util.ArrayList;
import java.util.List;

import com.yedam.app.info.MemInDAOImpl;
import com.yedam.app.info.MemberInfoDAO;
import com.yedam.app.info.MemberInfoVO;
import com.yedam.app.login.DAO;

public class EnDAOImpl extends DAO implements EnrolmentDAO {
	//extends 부모의 메소드를 그대로 사용할 수 있으며 오버라이딩 할 필요 없이 부모에 구현되있는 것을 직접 사용 가능하다.
	//implements의 가장 큰 특징은 이렇게 부모의 메소드를 반드시 오버라이딩(재정의)해야 한다.		
	//싱글톤 - 단 하나의 객체만 만들도록 보장해야하는 경우 단하나만 생성되어서 싱글톤
	private static EnrolmentDAO instance = null; 
	
	public static EnrolmentDAO getInstance() { //외부에서 호출가능
		if(instance == null)
				instance = new EnDAOImpl();
			return instance;
		}
	
	@Override
	public List<EnrolmentVO> selectAll() {
		List<EnrolmentVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM enrolment";
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				EnrolmentVO enVO = new EnrolmentVO();
				enVO.setLectureCode(rs.getInt("lecture_code"));
				enVO.setCoursePeriod(rs.getString("course_period"));
				enVO.setSubject(rs.getString("subject"));
				enVO.setTeacher(rs.getString("teacher"));
				enVO.setClassTime(rs.getString("class_time"));
				
				list.add(enVO);
			}
		}catch(Exception e) {
			e.printStackTrace();//Exception 이 발생한 이유와 위치는 어디에서 발생했는지 전체적인 단계를 다 출력합니다.
		}finally {
			disconnect();
		}
		return list;
	}



	
	
	
	
}
