package com.yedam.app.Enrolment;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.yedam.app.info.MemberInfoVO;
import com.yedam.app.login.DAO;
import com.yedam.app.login.LoginControl;
import com.yedam.app.members.Members;

//List는 순서가 있는 데이터들의 집합입니다. 불연속적인 메모리 공간에 데이터들이 연관되며 포인터를 통해 각 데이터들이 연결됩니다. 
//리스트는 동적으로 크기가 정해지며 데이터의 삽입, 삭제가 배열에 비해 용이하고 메모리의 재사용성이 높아집니다.

public class CourseImpl extends DAO implements MyClass{
	//extends 부모의 메소드를 그대로 사용할 수 있으며 오버라이딩 할 필요 없이 부모에 구현되있는 것을 직접 사용 가능하다.
	//implements의 가장 큰 특징은 이렇게 부모의 메소드를 반드시 오버라이딩(재정의)해야 한다.
	
	//싱글톤 - 단 하나의 객체만 만들도록 보장해야하는 경우 단하나만 생성되어서 싱글톤
	private static MyClass instance = null;
	//싱글톤 만들때 외부에서 호출하면 호출한 만큼 객체가 생성되기 때문에 생성자를 외부로 호출할수 없도록 'private'사용
	
	public static MyClass getInstance() { //외부에서 호출가능
		if(instance == null)
				instance = new CourseImpl();
			return instance;
		}


	//MyClass 오버라이딩
	
	@Override
	public void insert(CourseVO coVO) {
		try {//try-catch-finally 블록  일반 예외와 실행 예외가 발생할 경우 예외 처리를 할 수 있도록 해주는것
			//try 블록에는 예외 발생 가능 코드가 위치합니다.
			String pattern = "yyyy-MM-dd";//SimpleDateFormat을 이용하여 (ex 2022-11-21)형식으로 출력할수있게 도와줌
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String date = simpleDateFormat.format(new Date());
			
			connect();
			String sql = "Insert INTO myclass VALUES (?,?,?)";
			pstmt = conn.prepareStatement(sql);//
			pstmt.setInt(1, coVO.getLectureCode());
			pstmt.setString(2,(date));//날짜 표기할때 위와 같은 방법 사용시 setDate가 아닌 setString사용해야함!
			pstmt.setInt(3, coVO.getMem_no());
			int result=pstmt.executeUpdate();//execute 설명 맨 아래에 있음
			
			if(result > 0) { 
				System.out.println("정상적으로 수강신청이 되었습니다.");
			}else {
				System.out.println("정상적으로 수강신청이되지 않았습니다.");
			}
		}catch(Exception e) {//예외처리 try블록 코드가 예외 없이 발생하면 finally코드 실행 예외있으면 예외처리코드 실행
			System.out.println("이미신청된 강의입니다.");
			//e.printStackTrace();
		}finally {//finally블록
			disconnect();
		}

		
	}

	@Override
	public void delete(int lectureCode) {
		try {
			connect();
			stmt = conn.createStatement();//createStatement()	데이터베이스로 SQL 문을 보내기 위한 SQLServerStatement 개체를 만듭니다.
			String sql = "DELETE FROM myclass WHERE lecture_code = " + lectureCode;
			int result = stmt.executeUpdate(sql);
			if(result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			}else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();//Exception 이 발생한 이유와 위치는 어디에서 발생했는지 전체적인 단계를 다 출력합니다.
		}finally {
			disconnect();
		}
		
	}


	@Override
	public List<CourseVO> selectAll2() {
		List<CourseVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM myclass";
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				CourseVO coVO = new CourseVO();
				coVO.setLectureCode(rs.getInt("lecture_code"));
				coVO.setDate(rs.getString("date"));
				coVO.setMem_no(rs.getInt("mem_no"));
				
				list.add(coVO);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;
	}
}
////////////////Execute 설명////////////////////////
//Execute

//1. 수행결과로 Boolean 타입의 값을 반환합니다.
//2. 모든 구문을 수행할 수 있습니다.
//
//  -> 리턴값이 ResultSet 일 경우에는 true, 이 외의 경우에는 false 로 출력됩니다.
//  -> 리턴값이 ResultSet 이라고 하여 ResultSet 객체에 결과값을 담을 수 없습니다.

//ExecuteQuery
//1. 수행결과로 ResultSet 객체의 값을 반환합니다.
//2. SELECT 구문을 수행할 때 사용되는 함수입니다.

//ExecuteUpdate
//1. 수행결과로 Int 타입의 값을 반환합니다.
//2. SELECT 구문을 제외한 다른 구문을 수행할 때 사용되는 함수입니다.

