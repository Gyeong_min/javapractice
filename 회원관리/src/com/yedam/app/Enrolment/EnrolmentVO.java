package com.yedam.app.Enrolment;

public class EnrolmentVO {
	public int lectureCode ;
	private String subject;
	private String teacher;
	private String classTime;
	private String coursePeriod;
	public int getLectureCode() {
		return lectureCode;
	}
	public void setLectureCode(int lectureCode) {
		this.lectureCode = lectureCode;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public String getClassTime() {
		return classTime;
	}
	public void setClassTime(String classTime) {
		this.classTime = classTime;
	}
	public String getCoursePeriod() {
		return coursePeriod;
	}
	public void setCoursePeriod(String coursePeriod) {
		this.coursePeriod = coursePeriod;
	}
	@Override
	public String toString() {//정리를 하자면 우리는 클래스 내부에서 public String toString()라는 메소드를 선언해서 
//System.out.println(인스턴스의 이름)을 호출 했을 때 출력 될 문자열을 지정할 수 있는 것입니다.
		//EnDAOImpl에 전체 출력하도록 작성해서 필요는 없음 만약 이 중 몇개만 출력할시에는 출력할것만 return값에 넣어야함!
		return "EnrolmentVO [lectureCode=" + lectureCode + ", subject=" + subject + ", teacher=" + teacher
				+ ", classTime=" + classTime + ", coursePeriod=" + coursePeriod + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
