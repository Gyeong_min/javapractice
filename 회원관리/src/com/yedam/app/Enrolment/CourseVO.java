package com.yedam.app.Enrolment;



public class CourseVO {
	//필드
	public int lectureCode ;
	private String date;
	private int mem_no;

	

	public int getLectureCode() {
		return lectureCode;
	}
	public void setLectureCode(int lectureCode) {
		this.lectureCode = lectureCode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getMem_no() {
		return mem_no;
	}
	public void setMem_no(int mem_no) {
		this.mem_no = mem_no;
	}
	
	
	@Override
	public String toString() {
		return "CourseVO [lectureCode=" + lectureCode + ", date=" + date + ", mem_no=" + mem_no + "]";
	}
	
	

	
	
	
	
	
	
}




