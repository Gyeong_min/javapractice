package com.yedam.app.Enrolment;

import java.util.List;
import java.util.Scanner;

import com.yedam.app.info.MemberInfoVO;
import com.yedam.app.login.LoginControl;
import com.yedam.app.login.Management;
import com.yedam.app.members.Members;



public class EnrolmentManagement extends Management{
	

	Scanner sc = new Scanner(System.in);
	//- System.in은 화면에서 입력을 받겠다는 의미입니다. Scanner클래스는 화면 뿐만 아니라 파일로부터 입력을 받을 수 있는데 화면으로부터 정수형, 소수형과 같은 데이터 타입을 입력을 받을 것이기 때문에 System.in을 사용합니다.
	//- System.in은 입력한 값을 Byte 단위로 읽으며 키보드와 연결된 자바의 표준 입력 스트림입니다.
	EnrolmentDAO enDAO = EnDAOImpl.getInstance();
	MyClass coDAO = CourseImpl.getInstance(); //->public static MyClass getInstance()
	
	public EnrolmentManagement() {
		while(true) {
			//메뉴 출력
			menuPrint();
			//메뉴 선택
			int menuNo = menuSelect();
			
			//각 메뉴의 기능을 실행
			if(menuNo == 1){
				//전체조회
				selectAll();
			}else if(menuNo ==2) {
				//수강신청
				insertlecture();
			}else if(menuNo ==3) {
				//수강신청확인
				selectAll2();
			}else if(menuNo ==4) {
				//수강신청삭제
				deletelecture();
			}else if(menuNo ==5) {
				//종료
				end();
				break;
	
			}else {
				//기타사항
				printErrorMessage();
				
			}
		}
	}
	
	private void printErrorMessage() {
		System.out.println("======================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 한번 확인해주세요");
		System.out.println("======================");
	}
	private void end() {
		System.out.println("====================");
		System.out.println("프로그램을 종료합니다.");
		System.out.println("====================");
	}
	
	protected void menuPrint() {
		System.out.println("==================================================");
		System.out.println("1.전체조회 2.수강신청 3.전체신청확인 4.수강신청삭제 5.종료");
		System.out.println("==================================================");
	}
	
	protected int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어 있습니다.");
		}
		return menuNo;
	}
	
	private void selectAll() {
		List<EnrolmentVO> list = enDAO.selectAll();
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		for(EnrolmentVO enVO : list) {
			System.out.printf("%d: %s, %s, %s, %s \n", enVO.getLectureCode(), enVO.getSubject(), enVO.getTeacher(),enVO.getClassTime(), enVO.getCoursePeriod());
		}
	}
	private void selectAll2() {//수강신청 확인
		List<CourseVO> list = coDAO.selectAll2();
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		for(CourseVO coVO : list) {
			System.out.printf("강좌코드 %d, 신청날짜 %s, 회원번호 %d\n", coVO.getLectureCode(),coVO.getDate(),coVO.getMem_no());

		}
	}
	

	private void insertlecture() {//수강신청
		CourseVO coVO = inputCourseAll();//강좌번호 받아오기
		Members logininfo = LoginControl.getLoginInfo();//로그인한정보 가져오는것
		coVO.setMem_no(logininfo.getMemNo());//위 정보를 합친것
		coVO.setDate(coVO.getDate());//날짜
		coDAO.insert(coVO);
	}
	


	private void deletelecture() {//수강취소
		int coVO = inputcourseNo();
		coDAO.delete(coVO);
	}
	
	private CourseVO inputCourseAll() {
		CourseVO coVO = new CourseVO();
		System.out.println("강좌번호 입력 > ");
		coVO.setLectureCode(Integer.parseInt(sc.nextLine()));
		
		return coVO;
	}
	
	private int inputcourseNo() {
		int lectureCode = 0;
		try {
			System.out.println("강좌번호 > ");
			lectureCode = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("강좌는 숫자로 구성되어 있습니다.");
		}
		return lectureCode;
	}
	
	
}
